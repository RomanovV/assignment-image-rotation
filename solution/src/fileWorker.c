#include "fileWorker.h"

enum file_status file_open_read(char* const filename, FILE** filePointer) {
	*filePointer = fopen(filename,"rb");
	if (filePointer == NULL) return FILE_OPEN_ERROR;
	return FILE_OPEN;
}

enum file_status file_open_write(char* const filename, FILE** filePointer) {
	*filePointer = fopen(filename, "wb");
	if (filePointer == NULL) return FILE_OPEN_ERROR;
	return FILE_OPEN;
}

enum file_status file_close(FILE** filePointer) {
	int isClose = fclose(*filePointer);
	if (isClose != 0) return FILE_CLOSE_ERROR;
	return FILE_CLOSE;
}
