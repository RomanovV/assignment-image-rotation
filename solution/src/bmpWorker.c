#include "bmpWorker.h"

#define TYPE 19778
#define PLANES 1
#define SIZE 40
#define BIT_COUNT 24
#define HEADER_SIZE 54

static uint32_t find_padding(uint32_t width) {
  uint32_t padding;
  if(width % 4 == 0) {  
     padding = 0;   
  } else padding = 4*((width*3/4) + 1)-3*width;
  return padding;
}

enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header;
	if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER;
	img->width = header.biWidth;
	img->height = header.biHeight;
	img->data = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
	uint32_t const padding = find_padding(img->width);
	struct pixel* pixels = img->data;
	for (size_t i = 0; i < img->height; i++) {
		if (fread(pixels + img->width * i, sizeof(struct pixel), img->width, in) != img->width) {
			free(pixels);
			return READ_ERROR;
		}
		if (fseek(in, padding , SEEK_CUR) != 0) {
			free(pixels);
			return READ_ERROR;
		}
	}
	return READ_OK;
}

static size_t find_image_size(const struct image* img) {
	return find_padding(img->width)*img->height + sizeof(struct pixel) * img->width * img->height;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
	struct bmp_header header = (struct bmp_header){ 0 };
	size_t image_size = find_image_size(img);
	header.bfType = TYPE;
	header.bfReserved = 0;
	header.bfileSize = HEADER_SIZE + image_size;
	header.bOffBits = HEADER_SIZE;
	header.biSize = SIZE;
	header.biWidth = img->width;
	header.biHeight = img->height;
	header.biPlanes = PLANES;
	header.biBitCount = BIT_COUNT;
	header.biCompression = 0;
	header.biSizeImage = image_size;
	header.biXPelsPerMeter = 0;
	header.biYPelsPerMeter = 0;
	header.biClrUsed = 0;
	header.biClrImportant = 0;
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
	uint32_t const padding = find_padding(img->width);
	struct pixel* current_pixel = img->data;
	for (size_t i = 0; i < img->height; i++) {
		if (fwrite(current_pixel, sizeof(struct pixel), img->width, out) != img->width) return WRITE_ERROR;
		if (fwrite(current_pixel, 1, padding, out) != padding) return WRITE_ERROR;
		current_pixel = current_pixel + (size_t)img->width;
	}

	return WRITE_OK;
}

