#include "transform.h"
#include "imageWorker.h"

static uint64_t find_x(struct image img, size_t current_pixel) {
	return img.height - 1 - current_pixel / img.width;
}

static uint64_t find_y(struct image img, size_t current_pixel) {
	return current_pixel % img.width;
}

struct image rotate(struct image img) {
	uint64_t x;
	uint64_t y;
	uint64_t pixel_size = img.height * img.width;
	struct image result = image_create(img.height, img.width);
	result.data = malloc(sizeof(struct pixel) * pixel_size);
	for (size_t i = 0; i < pixel_size; i++) {
		x = find_x(img, i);
		y = find_y(img, i);
		result.data[y * img.height + x] = img.data[i];
	}
	return result;
}
