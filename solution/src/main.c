#include "bmpWorker.h"
#include "fileWorker.h"
#include "imageWorker.h"
#include "transform.h"
#include <stdio.h>
const char* write_status_out[] = {
	[WRITE_OK] = "Data wrote succesfully\n",
	[WRITE_ERROR] = "Error while writing img data\n"

};

const char* read_status_out[] = {
	[READ_OK] = "Data successfully read\n",
	[READ_ERROR] = "Error while read file\n",
	[READ_INVALID_HEADER] = "Error while reading the header\n"
};

const char* file_status_out[] = {
	[FILE_OPEN] = "File succesfully opened\n",
	[FILE_OPEN_ERROR] = "Error while file openning\n",
	[FILE_CLOSE] = "File succesfully closed\n",
	[FILE_CLOSE_ERROR] = "Error while file closing\n"
};

void status_message_output(const char* status) {
	fprintf(stderr, "%s", status);
}


static int execution_error(struct image* source, struct image* result) {
	image_delete(*result);
	image_delete(*source);
	return 1;
}

int main(int argc, char** argv) {

	if (argc != 3) {
		fprintf(stderr, "%s", "Incorrect command, input source and target images");
		return 1;
	}
	FILE* file;
	enum file_status f_status = file_open_read(argv[1], &file);
	status_message_output(file_status_out[f_status]);
	if (f_status == FILE_OPEN_ERROR) {
		return 1;
	}
	struct image source = { 0 };
	struct image result = { 0 };
	enum read_status r_status = from_bmp(file, &source);
	status_message_output(file_status_out[r_status]);
	if (r_status != READ_OK) {
		return execution_error(&source, &result);
	}
	f_status = file_close(&file);
	status_message_output(file_status_out[r_status]);
	if (f_status == FILE_CLOSE_ERROR) {
		return execution_error(&source, &result);
	}
	result = rotate(source);
	f_status = file_open_write(argv[2], &file);
	status_message_output(read_status_out[f_status]);
	if (r_status == FILE_OPEN_ERROR) {
		return execution_error(&source, &result);
	}
	enum write_status w_status = to_bmp(file, &result);
	status_message_output(write_status_out[w_status]);
	if (w_status != WRITE_OK) {
		return execution_error(&source, &result);
	}
	f_status = file_close(&file);
	status_message_output(file_status_out[r_status]);
	if (f_status == FILE_CLOSE_ERROR) {
		return execution_error(&source, &result);
	}
	image_delete(result);
	image_delete(source);
}
