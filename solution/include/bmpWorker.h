#include "bmp.h"
#include "status.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#ifndef BMP_WORKER
#define BMP_WORKER
enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image const* img);
#endif
