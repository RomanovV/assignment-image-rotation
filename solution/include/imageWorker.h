#include "image.h"
#include <stdint.h>
#include <stdlib.h>

#ifndef IMAGE_WORKER
#define IMAGE_WORKER
struct image image_create(const size_t width, const size_t height);

void image_delete(struct image img);
#endif
