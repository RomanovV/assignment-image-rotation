#ifndef STATUS
#define STATUS
/*  deserializer   */
enum read_status {
	READ_OK = 0,
	READ_ERROR,
	READ_INVALID_HEADER,
	/* ���� ������ ������  */
};

/*  serializer   */
enum  write_status {
	WRITE_OK = 0,
	WRITE_ERROR,
	/* ���� ������ ������  */
};

enum file_status {
	FILE_OPEN,
	FILE_OPEN_ERROR,
	FILE_CLOSE,
	FILE_CLOSE_ERROR
};
#endif
