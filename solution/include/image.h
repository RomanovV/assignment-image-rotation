#include <stdint.h>
#include <stdlib.h>
#ifndef IMAGE
#define IMAGE
struct pixel { uint8_t b, g, r; };

struct image {
	uint64_t width, height;
	struct pixel* data;
};
#endif 
