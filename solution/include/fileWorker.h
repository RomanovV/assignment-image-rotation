#ifndef FILE_WORKER
#define FILE_WORKER
#include "status.h"
#include "stdio.h"
enum file_status file_open_read(char* const filename, FILE** filePointer);
enum file_status file_open_write(char* const filename, FILE** filePointer);
enum file_status file_close( FILE** filePointer);
#endif
